$( document ).ready(function() {
    $('.brandTextTop').click(function(){
        window.location.href = 'http://localhost:8000/';
    });

    var submitButton = $('#submitButton');
    submitButton.click(function(){
        validateAndRegister();
    });

});

function validateAndRegister(){
    usernameInput = $('#signupUsername').val();
    passwordInput = $('#signupPassword').val();
    errorDiv = $('#loginError');
    errorDiv.html('');
    var usernameLen = usernameInput.length;
    var passwordLen = passwordInput.length;
    if (usernameLen < 3  || passwordLen < 3) {
        errorDiv.html('username and password must be at least 3 characters long');
    } else if ( usernameLen > 20){
        errorDiv.html('username must be less than 20 characters long');
    }
    var validLength = usernameLen >= 3 && passwordLen >= 3 && usernameLen < 20;
    // \w means A-Z, a-z, 0-9 and underscore
    var description = $.trim($("#signupDescription").val());
    var validUsername = usernameInput.match(/[^\w]/);
    var validDescription = description.match(/[^\w ]/);
    console.info(description);
    if(validUsername != null || validDescription  != null){
        errorDiv.html('username and description can only contain the following characters: A-Z, a-z, 0-9');
    }

    if(validLength && validUsername === null && validDescription === null){
        return $.post("http://localhost:8000/registerUser", {username:usernameInput, password:passwordInput, description:description }, function(result){
            console.info("data: " + result);
            if(result === ""){
                localStorage.setItem("username", usernameInput);
                window.location.href = 'http://localhost:8000/';
            } else {
                errorDiv.html(result);
            }
        });
    }

}
