// create a mongodb client and set up a the url
var mClient = require("mongodb").MongoClient,
url = "mongodb://localhost:27017/";

mClient.connect(url, {useNewUrlParser: true}, function(err, db){
    // recipedb
    var dbo = db.db("recipesharedb");

    ///////////////////////////////////////////
    /////////// RECIPES ///////////////////////
    ///////////////////////////////////////////

    var recipe_one = {"name":"Avocado on toast","type":["breakfast","lunch","snack"],"description":"Make this simple speedy breakfast with just a handful of ingredients. Our avocado toast uses crusty sourdough bread and a pinch of chilli for a kick","diet_info":["vegetarian","vegan"],"prep_time":"5 minutes","ingredients":["2 slices sourdough bread","big pinch chilli flakes","rip avocado","half lemon","drizzle extra virgin oil"],"method":["Cut the avocado in half and carefully remove its stone, then scoop out the flesh into a bowl. ","Squeeze in the lemon juice then mash with a fork to your desired texture. Season to taste with sea salt, black pepper and chilli flakes.","Toast your bread, drizzle over the oil then pile the avocado on top."],"image_source":"avocado-on-toast.jpg","source":"https://www.bbcgoodfood.com/recipes/avocado-toast"}


    // insert the recipe as a document
    dbo.collection("recipes").insertOne(recipe_one, function(err, res){
        console.log("Successfully added recipe to db");
    });

    // recipes
    var recipe_two = {"name":"Spiced carrot & lentil soup","type":["breakfast","lunch","dinner"],"description":"A delicious, spicy blend, packed full of iron and low fat to boot. It's ready in under half an hour or can be made in a slow cooker","diet_info":["vegetarian"],"prep_time":"15 minutes","ingredients":["2 tsp cumin seeds","pinch chilli flakes","2 tbsp olive oil","600g carrots Carrot , washed and coarsely grated (no need to peel)","140g split red lentils","1l hot vegetable stock (from a cube is fine)","125ml milk","plain yogurt yogurt and naan bread, to serve"],"method":["Heat a large saucepan and dry-fry 2 tsp cumin seeds and a pinch of chilli flakes for 1 min, or until they start to jump around the pan and release their aromas.","Scoop out about half with a spoon and set aside. Add 2 tbsp olive oil, 600g coarsely grated carrots, 140g split red lentils, 1l hot vegetable stock and 125ml milk to the pan and bring to the boil. ","Simmer for 15 mins until the lentils have swollen and softened.","Whizz the soup with a stick blender or in a food processor until smooth (or leave it chunky if you prefer).","Season to taste and finish with a dollop of plain yogurt and a sprinkling of the reserved toasted spices. Serve with warmed naan breads."],"image_source":"spice-carrot-lentil-soup.jpg","source":"https://www.bbcgoodfood.com/recipes/2089/spiced-carrot-and-lentil-soup"}

    // insert the recipe as a document
    dbo.collection("recipes").insertOne(recipe_two, function(err, res){
        console.log("Successfully added recipe to db");
    });

    // recipedb
    var recipe_three = {"name":"Creamy courgette lasagne","type":["dinner"],"description":"Serve up this creamy quick dish for a last minute dinner party and impress veggie friends","diet_info":["vegetarian"],"prep_time":"30 minutes","ingredients":["9 dried lasagne sheets","1 tbsp sunflower oil Sunflower oil","1 onion Onion  , finely chopped","700g courgette Courgette  (about 6), coarsely grated","2 garlic cloves, crushed","250g tub ricotta Ricotta","50g cheddar Cheddar","350g jar tomato sauce for pasta"],"method":["Heat oven to 220C/fan 200C/gas 7. Put a pan of water on to boil, then cook the lasagne sheets for about 5 mins until softened, but not cooked through. Rinse in cold water, then drizzle with a little oil to stop them sticking together.","Meanwhile, heat the oil in a large frying pan, then fry the onion. After 3 mins, add the courgettes and garlic and continue to fry until the courgette has softened and turned bright green. Stir in 2/3 of both the ricotta and the cheddar, then season to taste. Heat the tomato sauce in the microwave for 2 mins on High until hot.","In a large baking dish, layer up the lasagne, starting with half the courgette mix, then pasta, then tomato sauce. Repeat, top with blobs of the remaining ricotta, then scatter with the rest of the cheddar. Bake on the top shelf for about 10 mins until the pasta is tender and the cheese is golden."],"image_source":"creamy-courgette-lasagne.jpg","source":"https://www.bbcgoodfood.com/recipes/4716/creamy-courgette-lasagne"}
   dbo.collection("recipes").insertOne(recipe_three, function(err, res){
        console.log("Successfully added recipe to db");
        db.close();
    });

    var recipe_four= {"name":"Spicy root & lentil casserole","description":"The potatoes in this recipe take on the spicy flavours beautifully - our idea of the perfect veggie supper","diet_info":["vegetarian"],"prep_time":"45 minutes","ingredients":["2 tbsp sunflower or vegetable oil","1 onion  , chopped","2 garlic clove, crushed","700g potatoes  , peeled and cut into chunks","4 carrot  , thickly sliced","2 parsnip  , thickly sliced","2 tbsp curry paste or powder","1 litre/1¾ pints vegetable stock","100g red lentils","a small bunch of fresh coriander, roughly chopped","low-fat yogurt   and naan bread, to serve"],"method":["Heat the oil in a large pan and cook the onion and garlic over a medium heat for 3-4 minutes until softened, stirring occasionally. Tip in the potatoes, carrots and parsnips, turn up the heat and cook for 6-7 minutes, stirring, until the vegetables are golden.","Stir in the curry paste or powder, pour in the stock and then bring to the boil. Reduce the heat, add the lentils, cover and simmer for 15-20 minutes until the lentils and vegetables are tender and the sauce has thickened.","Stir in most of the coriander, season and heat for a minute or so.Top with yogurt and the rest of the coriander. Serve with naan bread."],"image_source":"spicy-root-lentil-casserole.jpg","source":"https://www.bbcgoodfood.com/recipes/1364/spicy-root-and-lentil-casserole"}
   dbo.collection("recipes").insertOne(recipe_four, function(err, res){
        console.log("Successfully added recipe to db");
        db.close();
    });

    var recipe_five= {"name":"Mustard-stuffed chicken","type":["dinner"],"description":"This is so good we'd be surprised if this chicken fillet recipe doesn't become a firm favourite.","diet_info":["poultry"],"prep_time":"30 minutes","ingredients":["125g ball mozzarella, torn into small pieces","50g strong cheddar  , grated","1 tbsp wholegrain mustard","4 skinless boneless chicken breast fillets","8 smoked streaky bacon rashers"],"method":["Heat oven to 200C/fan 180C/gas 6. Mix the cheeses and mustard together. Cut a slit into the side of each chicken breast, then stuff with the mustard mixture.","Wrap each stuffed chicken breast with 2 bacon rashers – not too tightly, but enough to hold the chicken together.","Season, place on a baking sheet and roast for 20-25 mins."],"image_source":"mustard-stuffed-chicken.jpg","source":"https://www.bbcgoodfood.com/recipes/4814/mustardstuffed-chicken"}
   dbo.collection("recipes").insertOne(recipe_five, function(err, res){
        console.log("Successfully added recipe to db");
        db.close();
    });

    var recipe_six= {"name":"Raspberry Bakewell cake","type":["dessert"],"description":"This simple almondy cake is a great way of using up pick-your-own raspberries","diet_info":["freezable"],"prep_time":"60 minutes","ingredients":["140g ground almond","140g butter  , softened","140g golden caster sugar","140g self-raising flour","2 eggs","1 tsp vanilla extract","250g raspberry","2 tbsp flaked almond","icing sugar, to serve"],"method":["Heat oven to 180C/160C fan/gas 4 and base-line and grease a deep 20cm loose-bottomed cake tin. Blitz the ground almonds, butter, sugar, flour, eggs and vanilla extract in a food processor until well combined.","Spread half the mix over the cake tin and smooth over the top. Scatter the raspberries over, then dollop the remaining cake mixture on top and roughly spread – you might find this easier to do with your fingers.","Scatter with flaked almonds and bake for 50 mins until golden. Cool, remove from the tin and dust with icing sugar to serve."],"image_source":"raspberry-backwell-cake.jpg","source":"https://www.bbcgoodfood.com/recipes/11695/raspberry-bakewell-cake"}
   dbo.collection("recipes").insertOne(recipe_six, function(err, res){
        console.log("Successfully added recipe to db");
        db.close();
    });

    var recipe_seven= {"name":"Chocolate fondant","type":["dessert"],"description":"A gooey prepare-ahead dessert that's perfect for entertaining - it's all a matter of timing...","diet_info":["Can be frozen uncooked"],"prep_time":"60 minutes","ingredients":["50g melted butter, for brushing","cocoa powder, for dusting","200g good-quality dark chocolate  , chopped into small pieces","200g butter, in small pieces","200g golden caster sugar","4 eggs and 4 yolks","200g plain flour","Caramel sauce and vanilla   ice cream or orange sorbet, to serve"],"method":["Heat oven to 180C/160C fan/gas 4 and base-line and grease a deep 20cm loose-bottomed cake tin. Blitz the ground almonds, butter, sugar, flour, eggs and vanilla extract in a food processor until well combined.","Spread half the mix over the cake tin and smooth over the top. Scatter the raspberries over, then dollop the remaining cake mixture on top and roughly spread – you might find this easier to do with your fingers.","Scatter with flaked almonds and bake for 50 mins until golden. Cool, remove from the tin and dust with icing sugar to serve."],"image_source":"chocolate-fondant.jpg","source":"https://www.bbcgoodfood.com/recipes/8168/chocolate-fondant"}
   dbo.collection("recipes").insertOne(recipe_seven, function(err, res){
        console.log("Successfully added recipe to db");
        db.close();
    });

    var recipe_eight= {"name":"Double chocolate loaf cake","type":["dessert"],"description":"Chocolate and cake are two of our favourite things, so what's not to love about this indulgent cake?","diet_info":["Can be frozen un-iced"],"prep_time":"120 minutes","ingredients":["175g softened butter, plus extra for greasing","175g golden caster sugar","3 eggs","140g self-raising flour","85g ground almonds","½ tsp baking powder","100ml milk","4 tbsp cocoa powder","50g plain chocolate chip or chunks","few extra chunks white, plain and milk chocolate, for decorating"],"method":["Heat oven to 160C/140C fan/gas 3. Grease and line a 2lb/900g loaf tin with a long strip of baking parchment. To make the loaf cake batter, beat the butter and sugar with an electric whisk until light and fluffy","Beat in the eggs, flour, almonds, baking powder, milk and cocoa until smooth. Stir in the chocolate chips, then scrape into the tin.","Bake for 45-50 mins until golden, risen and a skewer poked in the centre comes out clean.","Cool in the tin, then lift out onto a wire rack over some kitchen paper. Melt the extra chocolate chunks separately in pans over barely simmering water, or in bowls in the microwave, then use a spoon to drizzle each in turn over the cake. Leave to set before slicing."],"image_source":"double-chocolate-loaf-cake.jpg","source":"https://www.bbcgoodfood.com/recipes/694633/double-chocolate-loaf-cake"}
   dbo.collection("recipes").insertOne(recipe_eight, function(err, res){
        console.log("Successfully added recipe to db");
        db.close();
    });

    var recipe_nine= {"name":"Smoked haddock & leek risotto","type":["dinner"],"description":"There's no tedious stirring with this one - just stick it in the oven until creamy and delicious","diet_info":["pescatarian"],"prep_time":"40 minutes","ingredients":["small knob of butter","1 large leek, thinly sliced","300g risotto rice, such as arborio or carnaroli","700ml fish or vegetable stock","250ml full-fat milk","375g undyed smoked haddock, skinned and cut into large chunks","3 tbsp crème fraîche","100g baby spinach"],"method":["Heat oven to 200C/180C fan/gas 6. Heat the butter in a large ovenproof dish over a medium heat. Cook the leek for 4-5 mins, stirring regularly, until just tender. Add the rice and stir for a further 2 mins.","Add the stock and milk, bring to the boil and bubble for 5 mins before sitting the haddock on top. Cover with a lid or foil and bake in the oven for 18 mins until the rice is tender.","Fold in the crème fraîche and spinach, season with plenty of black pepper, then cover the pan again and leave to rest out of the oven for 3 mins before serving – the steam will soften the spinach."],"image_source":"smoked-haddock-leek-risotto.jpg","source":"https://www.bbcgoodfood.com/recipes/408651/smoked-haddock-and-leek-risotto"}
   dbo.collection("recipes").insertOne(recipe_nine, function(err, res){
        console.log("Successfully added recipe to db");
        db.close();
    });

     ///////////////////////////////////////////
     /////////// USERS ///////////////////////
     ///////////////////////////////////////////

    var user_one = {"first_name":"Tanaka","last_name":"Mudimu","username":"Tanaka","password":"tanaka123","description":"Student at Plymouth University who loves to explore different types of food."}

    dbo.collection("users").insertOne(user_one, function(err, res){
        console.log("Successfully added user to db");
    });

    var user_two = {"first_name":"Juliet","last_name":"Montgomery","username":"Juliet","password":"juliet123","description":"Self-proclaimed 'elite foodie' looking for more recipes to challenge my taste buds."}

    dbo.collection("users").insertOne(user_two, function(err, res){
        console.log("Successfully added user to db");
    });

    var user_three = {"first_name":"Jim","last_name":"Barns","username":"Jim","password":"jim123","description":"Aspiring chef in Plymouth University."}

    dbo.collection("users").insertOne(user_three, function(err, res){
        console.log("Successfully added user to db");
        db.close();
    });
});
