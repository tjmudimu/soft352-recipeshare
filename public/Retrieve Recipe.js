function retrieveRecipes(collection, user) {
    var currentUser = "";

    if(user === 0){
        currentUser = localStorage.getItem("username");
    } else {
        currentUser = user;
    }
    if(currentUser === null && collection != "editorchoices"){
        return;
    }

    switch(collection){
        case "editorchoices": return $.get("http://localhost:8000/editorchoices", function(data, status){
             data.forEach(addEditorChoices);
        });
        case "user_recs":return $.get("http://localhost:8000/recommended", { username : currentUser }, function(data, status){
            if(data === ""){
                noRecipesTiles("recommendedContainerScroller");
                return;
            }
            data.forEach(addRecommended);
        });
        case "user_favs":return $.get("http://localhost:8000/favourites", { username : currentUser }, function(data, status){
            if(data === ""){
                noRecipesTiles("favouritesContainerScroller");
                return;
            }
            data.forEach(addFavourites);
        });
        case "custom":return $.get("http://localhost:8000/getRecipes", { searchQuery : user }, function(data, status){
            if(data === ""){
                noRecipesTiles("searchContainerScroller");
                return;
            }
            data.forEach(addSearched);
        });
        default: return $.get("http://localhost:8000/saves", { username : currentUser }, function(data, status){
            if(data === ""){
                noRecipesTiles("savedContainerScroller");
                return;
            }
            data.forEach(addSaves);
        });
    }
}

function addEditorChoices(recipe){
  var recipeId = recipe["id"];
  var typeid = "editChoice";
  performTileAdd(recipe, typeid, "editorChoiceContainerScroller");
}

function addRecommended(recipe){
  var recipeId = recipe["id"];
  var typeid = "rec";
  performTileAdd(recipe, typeid, "recommendedContainerScroller");
}

function addFavourites(recipe){
  var recipeId = recipe["id"];
  var typeid = "fav";
  performTileAdd(recipe, typeid, "favouritesContainerScroller");
}

function addSaves(recipe){
  var typeid = "save";
  performTileAdd(recipe, typeid, "savedContainerScroller");
}

function addSearched(recipe){
  var typeid = "searched";
  var recipeId = recipe["id"];
  $('#searchContainerScroller')
      .append(customTileHTML(recipe, typeid));
  $("#recipeTile"+typeid+recipeId).click(function(){redirectToRecipe(recipeId)});
}

function performTileAdd(recipe, typeid, scroller){
    var recipeId = recipe["id"];
    $('#'+scroller)
        .append(recipeTileHTML(recipe, typeid));
    $("#recipeTile"+typeid+recipeId).click(function(){redirectToRecipe(recipeId)});
}


function pleaseLogIn(){
    noRecipesTilesLogin("followRecommendContainerScroller", "followed user");
    noRecipesTilesLogin("recommendedContainerScroller", "recommended");
    noRecipesTilesLogin("favouritesContainerScroller", "favourited");
    noRecipesTilesLogin("savedContainerScroller", "saved");
}

function redirectToRecipe(recipe_id){
  console.info("redirect called");
    window.location.href = 'http://localhost:8000/viewRecipe?recipe_id='+recipe_id;
}

function recipeTileHTML(recipe, typeid){
  var recipeId = recipe["id"];
  var html = '<div class="recipeTile" id="recipeTile'+typeid+recipeId+'">'
          + '<img id="tileImage" src="recipe_images/'+recipe["image_source"]+'" alt="'+recipe["name"]+'">'
          + '<div id="tileRecipeName">'
          + recipe["name"]
          + '</div>'
          + '<div id="tileDescription">'
          + recipe["description"]
          + '</div>'
          + '<div id="tilePrepTime"> ready in '
          + recipe["prep_time"]
          + '</div>'
          + '<div id="tileDiet">'
          + " ("
          + recipe["diet_info"]
          + ")"
          + '</div>'
          + '</div>';
  return html;
}

function customTileHTML(recipe, typeid){
  var recipeId = recipe["id"];
  var html = '<div class="searchTile" id="recipeTile'+typeid+recipeId+'">'
          + '<img id="tileImage" src="recipe_images/'+recipe["image_source"]+'" alt="'+recipe["name"]+'">'
          + '<div id="tileRecipeName">'
          + recipe["name"]
          + '</div>'
          + '<div id="tileDescription">'
          + recipe["description"]
          + '</div>'
          + '<div id="tilePrepTime"> ready in '
          + recipe["prep_time"]
          + '</div>'
          + '<div id="tileDiet">'
          + " ("
          + recipe["diet_info"]
          + ")"
          + '</div>'
          + '</div>';
  return html;
}

function noRecipesTiles(scroller){
    var html = '<div class="placeholderRecipeTile"></div>'
    + '<div class="placeholderRecipeTile">No recipes currently</div>'
    + '<div class="placeholderRecipeTile"></div>';
    $('#'+scroller).append(html);
}

function noRecipesTilesLogin(scroller, type){
    var html = '<div class="noLoginRecipeTile"></div>'
    + '<div class="noLoginRecipeTile">Please log in to see '+type+' recipes</div>'
    + '<div class="noLoginRecipeTile"></div>';
    $('#'+scroller).append(html);
}
