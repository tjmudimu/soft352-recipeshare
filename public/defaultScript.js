$( document ).ready(function() {
    $('.brandTextTop').click(function(){
        window.location.href = 'http://localhost:8000/';
    });
    var searchBar = $('#searchBar');
    searchBar.keypress(function(e){
        var keyCode = e.which;
        // 13 === [enter] key
        if(keyCode === 13){
            window.location.href = 'http://localhost:8000/searchRecipes?searchQuery='+searchBar.val();
        }
    });
    var searchButton = $("#searchButton");
    searchButton.click(function(){
        window.location.href = 'http://localhost:8000/searchRecipes?searchQuery='+searchBar.val();
    });

    /// search page search bar
    var newSearchBar = $('#newSearchBar');
    if(newSearchBar != null && newSearchBar != undefined ){
        var scroller = $('#searchContainerScroller');
        newSearchBar.keypress(function(e){
            var keyCode = e.which;
            // 13 === [enter] key
            if(keyCode === 13){
                scroller.empty();
                retrieveRecipes("custom", newSearchBar.val());
            }
        });
        var newSearchButton = $("#newSearchButton");
        newSearchButton.click(function(){
            scroller.empty();
            retrieveRecipes("custom", newSearchBar.val());
        });
    }

    $('#userSearchButton').click(function(){
        window.location.href = 'http://localhost:8000/searchUsers';
    });

});
