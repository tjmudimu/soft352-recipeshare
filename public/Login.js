var activeUser = localStorage.getItem("username");

$( document ).ready(function() {
    if(activeUser != null){
        connectToServer(activeUser);
    }
    $('.submitLoginButton').click(function(){
        attemptLogin();
    });
    $('.cancelLoginButton').click(function(){
        toggleLoginContainer();
    });
    var passwordBar = $('#loginPassword');
    passwordBar.keypress(function(e){
        var keyCode = e.which;
        // 13 === [enter] key
        if(keyCode === 13){
            attemptLogin();
        }
    });
    var usernameBar = $('#loginUsername');
    usernameBar.keypress(function(e){
        var keyCode = e.which;
        // 13 === [enter] key
        if(keyCode === 13){
            attemptLogin();
        }
    });
});

function userExists(){
    return activeUser !== null;
}

function renderButtons(){
    var value = "";
    if(userExists()){
        value = '<div class="topUsername">logged in as <strong>'+ activeUser +'</strong></div>'
        +'<button id="LoginButton" class="profileButton">Profile</button>'
        +'<button id="userSearchButton">Search Users</button>'
        +'<button id="LoginButton" class="LogoutButton" >Log out</button>';
    } else {
        value = '<button id="userSearchButton">Search Users</button>'
        +'<button id="registerButton">Register</button>'
        +'<button id="LoginButton">Log in</button>';
    }

    $(".topButtonContainer").append(value);
    if(userExists()){
        $('.LogoutButton').click(function(){
            logOut();
            location.reload();
        });
        $('.profileButton').click(function(){
            window.location.href = 'http://localhost:8000/viewUser?username='+activeUser;
        });
    } else {
        $('#LoginButton').click(function(){
            toggleLoginContainer();
        });
        $('.topUsername').attr("display", "none");
        $('#registerButton').click(function(){
            window.location.href = 'http://localhost:8000/userRegistration';
        });
    }

}

function attemptLogin(){
    var user = $("#loginUsername").val();
    var pass = $("#loginPassword").val();
    return $.post("http://localhost:8000/login", {username:user, password:pass},    function(result){
        console.info("data: " + result);
        if(result === ""){
            successfulLogin(user);
            connectToServer(user)
            location.reload();
        } else {
            $('#loginError')[0].innerHTML = result;
        }
    });

}

function successfulLogin(user){
    localStorage.setItem("username", user);
    toggleLoginContainer();
}

function logOut(){
    localStorage.removeItem("username");
}

function toggleLoginContainer(){
    $("#loginContainer").toggle();
}

function connectToServer(username){
    var clientSocket = new WebSocket("ws://localhost:8000");

    clientSocket.onopen = function (event) {
        // Set up an event handler to process messages received
        // from a server .
        clientSocket.onmessage = function(message){
            jObject = JSON.parse(message.data);
            if(jObject.username != null && jObject.username != undefined){
                console.info(message.data);
                notify(jObject);
            } else {
                // generic connection success message
            }
        };
        // When the connection is established , send the server
        // a message .
        message = {username:username, message:"connected"};

        clientSocket.send(JSON.stringify(message));
    };
}

var offsetDelta = 4.5;
var popupPositionOffset = offsetDelta*-1;

function notify(notificationObject){
    var recipe = notificationObject.recipe_id;
    var notifyingUser = notificationObject.username

    if($("#"+notifyingUser+recipe).length != 0) {
        return;
    }

    popupPositionOffset = popupPositionOffset + offsetDelta;

    $(document.body).append('<div style="top:'+(8+popupPositionOffset)+'rem" class="notificationPopUp" id="'+notifyingUser+recipe+'"></div>');

    var notification = $("#"+notifyingUser+recipe);
    notification.html(notifyingUser + " just recommended a recipe!<br>Click here to go to the recipe");
    notification.toggleClass("show");
    var removalTimeout;
    var fadeoutTimeout = setTimeout(function (){
        notification.toggleClass("fadeout");
        removalTimeout = setTimeout(function (){
            notification.remove();
            if(popupPositionOffset > (offsetDelta*-1)){
                popupPositionOffset = popupPositionOffset - offsetDelta;
            }
        }, 6000);
    }, 3500);

    var showTimeout = setTimeout(function (){
        notification.toggleClass("show");
    }, 8900);

    notification.off("click").click(function(){
        window.location.href = 'http://localhost:8000/viewRecipe?recipe_id=' + recipe ;
    })

    notification.hover(function() {
        clearTimeout(fadeoutTimeout);
        clearTimeout(removalTimeout);
        clearTimeout(showTimeout);
        // notification.toggleClass("fadeout");
        if(!notification.hasClass("show")){
            notification.toggleClass("show");
        }
        if(notification.hasClass("fadeout")){
            notification.toggleClass("fadeout");
        }
    }, function() {
        notification.toggleClass("fadeout");
        if(notification.hasClass("show")){
            notification.toggleClass("show");
        }
        removalTimeout = setTimeout(function (){
            notification.remove();
            if(popupPositionOffset > (offsetDelta*-1)){
                popupPositionOffset = popupPositionOffset - offsetDelta;
            }
        }, 6000);

    });
}
