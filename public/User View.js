var activeUser = localStorage.getItem("username");
var userBeingViewed = "";

function disableButtons(){
  $('#uvbFollow').prop('disabled', true);
  $(".buttonWarning").show();
}

function enableButtons(){
  $('#uvbFollow').prop('disabled', false);
  $(".buttonWarning").hide();
}

function processOperation(userToFollow, type){
  console.log("user: "+userToFollow+" operation: "+type);
  return $.post("http://localhost:8000/"+type, {username:activeUser, userToFollow:userToFollow}, function(result){
      initialiseButton(userBeingViewed);
  });
}

var followers = [];
var following = [];
var initCounter = 0;
function initialiseButton(userToFollow){
    if(userBeingViewed === ""){
        userBeingViewed = userToFollow;
    }
    $.post("http://localhost:8000/userFollowCheck", {username:activeUser, userToFollow:userToFollow}, function(result){
        if(result.following){
            $('#uvbFollow').html('Following');

            $('#uvbFollow').hover(function() {
                $('#uvbFollow').html("Unfollow");
            }, function() {
                $('#uvbFollow').html("Following");
            });
            $('#uvbFollow').off("click").click(function(){
                initCounter++;
                console.info("follow button clicks: "+ initCounter);
                processOperation(userToFollow, "unfollowUser");
            });
        } else {
            $('#uvbFollow').hover(function() {
                $('#uvbFollow').html("Follow");
            }, function() {
                $('#uvbFollow').html("Follow");
            });
            $('#uvbFollow').html('Follow');
            $('#uvbFollow').off("click").click(function(){
                initCounter++;
                console.info("follow button clicks: "+ initCounter);
                processOperation(userToFollow, "followUser");
            });
        }
        following = result.followInfo.followData.follows;
        followers = result.followInfo.followData.followedBy;
        retrieveFollowInfo();
    });
    // If there is a user signed in, check if the user being looked at is already followed
  if(activeUser != null && activeUser != undefined){
      enableButtons();
      if(activeUser === userToFollow){
          $(".buttonWarning").text("You cannot follow yourself");
          disableButtons();
      }
  } else {
      $('.buttonWarning').html('Please log in to follow this user');
      $('#uvbFollow').off("click").click(function(){
        processOperation(userToFollow, "followUser");
      });
  }

}

function retrieveFollowInfo(){
    var i;
    $('#followersList').empty();
    $('#followingList').empty();
    for (i = 0; i < followers.length; ++i) {
        var username1 = followers[i];
        var onclickCode = 'onclick="javascript:window.location.href = \'http://localhost:8000/viewUser?username='+username1+'\';"';
        $('#followersList').append('<li class="followersLi" id="follower_'+username1+'" '+onclickCode+'>'+username1+'</li>');

    }

    var j;
    for (j = 0; j < following.length; ++j) {
        var username = following[j];
        var onclickCode = 'onclick="javascript:window.location.href = \'http://localhost:8000/viewUser?username='+username+'\';"';
        $('#followingList').append('<li class="followersLi" id="following_'+username+'" '+onclickCode+'>'+username+'</li>');
    }

    $("#followersHeader").html("Followers ("+followers.length+")");
    $("#followingHeader").html("Following ("+following.length+")");
}
