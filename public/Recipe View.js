var activeUser = localStorage.getItem("username");
$( document ).ready(function() {
  if(activeUser != null){
    enableButtons();
  }

});

function disableButtons(){
  $('#rvbSave').prop('disabled', true);
  $('#rvbFav').prop('disabled', true);
  $('#rvbRec').prop('disabled', true);
  $(".buttonWarning").show();
}

function enableButtons(){
  $('#rvbSave').prop('disabled', false);
  $('#rvbFav').prop('disabled', false);
  $('#rvbRec').prop('disabled', false);
  $(".buttonWarning").hide();
}

function processRecipe(recipe_id, type){
  console.log(recipe_id);
  return $.post("http://localhost:8000/"+type, {username:activeUser, recipe_id:recipe_id}, function(result){
      console.info("reinitialising recipe view buttons");
      initialiseButtons(recipe_id);
  });
}

function initialiseButtons(recipe_id){

    // If there is a user signed in, check if the recipe is saved/favourited/recommended
  if(activeUser != null && activeUser != undefined){
      return $.post("http://localhost:8000/userRecipeCheck", {username:activeUser, recipe_id:recipe_id}, function(result){
          if(result["saved"]){
              $('#rvbSave').html('Saved');

              $('#rvbSave').hover(function() {
                  $('#rvbSave').html("Unsave");
              }, function() {
                  $('#rvbSave').html("Saved");
              });
              $('#rvbSave').off('click').click(function(){
                  processRecipe(recipe_id, "unsaveRecipe");
              });
          } else {
              $('#rvbSave').html('Save');
              $('#rvbSave').unbind('mouseenter mouseleave');
              $('#rvbSave').off('click').click(function(){
                processRecipe(recipe_id, "saveRecipe");
              });
          }
          if(result["favourited"]){
              $('#rvbFav').html('Favourited');

              $('#rvbFav').hover(function() {
                  $('#rvbFav').html("Unfavourite");
              }, function() {
                  $('#rvbFav').html("Favourited");
              });
              $('#rvbFav').off('click').click(function(){
                  processRecipe(recipe_id, "unfavouriteRecipe");
              });
          } else {
              $('#rvbFav').html('Favourite');
              $('#rvbFav').unbind('mouseenter mouseleave');
              $('#rvbFav').off('click').click(function(){
                processRecipe(recipe_id, "favouriteRecipe");
              });
          }

          if(result["recommended"]){
              $('#rvbRec').html('Recommended');

              $('#rvbRec').hover(function() {
                  $('#rvbRec').html("Unrecommend");
              }, function() {
                  $('#rvbRec').html("Recommended");
              });
              $('#rvbRec').off('click').click(function(){
                  processRecipe(recipe_id, "unrecommendRecipe");
              });
          } else {
              $('#rvbRec').html('Recommend');
              $('#rvbRec').unbind('mouseenter mouseleave');
              $('#rvbRec').off('click').click(function(){
                processRecipe(recipe_id, "recommendRecipe");
              });
          }
      });
  } else {
      // set initial 'click' event handlers for each button before checking for a user
      $('#rvbSave').off('click').click(function(){
        processRecipe(recipe_id, "saveRecipe");
      });
      $('#rvbFav').off('click').click(function(){
        processRecipe(recipe_id, "favouriteRecipe");
      });
      $('#rvbRec').off('click').click(function(){
        processRecipe(recipe_id, "recommendRecipe");
      });
  }

}
