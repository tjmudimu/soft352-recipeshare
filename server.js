const http = require('http'),
express = require('express'),
app = new express(),
hostname = '127.0.0.1',
port = 8000,
path = require('path'),
bodyparser = require('body-parser');

//////// MONGOOSE SETUP

var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/recipesharedb", { useNewUrlParser: true });
var mongooseCon = mongoose.connection;

mongooseCon.on('error', console.error.bind(console, 'Error connecting to mongodb:'));
mongooseCon.once('open', function () {
    console.log("Mongoose successfully connected");
});

// Prepare a schema and model for recipes
var recipeSchema = mongoose.Schema({
    name: String,
    type: [String],
    description: String,
    diet_info: [String],
    prep_time: String,
    ingredients: [String],
    method: [String],
    image_source: String,
    source: String

}, { collection : 'recipes' });

var recipeModel = mongoose.model('recipe', recipeSchema)

// Prepare a schema and model for users
var userSchema = mongoose.Schema({
    first_name: String,
    last_name: String,
    username: String,
    password: String,
    description: String,
    favourites: [String],  // array of recipe_ids
    saved: [String], // array of recipe_ids
}, { collection : 'users' });

var userModel = mongoose.model('user', userSchema);

// Prepare a schema and model for editor choices
var editorChoiceSchema = mongoose.Schema({
    recipe_id: String
}, { collection : 'editorchoices' });

var editorChoiceModel = mongoose.model('editorChoice', editorChoiceSchema);

// Prepare a schema and model for user followers
var userFollowsSchema = mongoose.Schema({
    username: String, // String: follower
    followData: {follows: [String], followedBy: [String]}
}, { collection : 'userfollows' });

var userFollowsModel = mongoose.model('userFollow', userFollowsSchema);

// Prepare a schema and model for user follows
var userRecommendationSchema = mongoose.Schema({
    username: String, // list of recommended recipe ids
    recommendations:[String]
}, { collection : 'userrecommendations' });

var userRecommendationModel = mongoose.model('userRecommendation', userRecommendationSchema);

// path to the 'public' folder
var publicPath = path.join(__dirname, 'public');

// parse application/x-www-form-urlencoded
app.use(bodyparser.urlencoded({ extended: false }))

// parses request text as json and the result is placed in request.body
app.use(bodyparser.json())

app.use(express.static(publicPath));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', publicPath);

/// Setting up web sockets
const WSServer = require ("websocket").server ;

// const server = http.createServer(app);

var server = app.listen(port, function(){
    console.log('Listening on port ' + port);
})

var socket = new WSServer ({
    httpServer: server
});

var connections = [ ];

socket.on("request", function(request){
    var connection = request.accept(null, request.origin);
    connectionTimeoutToggle(true, connection);

    connection.on("close", function(reasonCode, description){
        connectionTimeoutToggle(false, connection);
    });

    connection.on("message", function(message){
        jObject = JSON.parse(message.utf8Data);
        if(jObject != null && jObject != undefined){
            processConnectionObject(jObject, connection)
            msg = JSON.stringify({message: "Successfully connected to server " + jObject.username});
            connection.sendUTF(msg);
        } else {
            msg = JSON.stringify({message: "Server: Received empty message"});
            connection.sendUTF(msg);
        }
    });
});

function processConnectionObject(jObject, connection){
    username = jObject.username;
    message = jObject.message;

    // pull any existing connections from this user first
    var newCon = pullConnection(username);
    if(newCon){
        console.log(username + " just connected to the server");
    }

    if(message=== "connected"){
        connections.push({username: username, connection: connection});
    }
}

function pullConnection(username){
    var newConnection = true;
    removalIndex = null;
    for(i = 0; i < connections.length; i++){
        if(connections[i].username === username){
            removalIndex = i;
            break;
        }
    }
    if(removalIndex != null){
        connections.splice(removalIndex, 1);
        newConnection = false;
    }
    return newConnection;
}

function identifyTimeoutConnection(connection){
    for(i = 0; i < connections.length; i++){
        if(connections[i].connection === connection){
            console.log(connections[i].username + " has disconnected from the server");
            break;
        }
    }
}

var connectionTimeout;

function connectionTimeoutToggle(clear, connection){
    if(clear){
        clearTimeout(connectionTimeout);
    } else {
        connectionTimeout = setTimeout(function(){
            identifyTimeoutConnection(connection);
        }, 3000);
    }
}

app.get('/', function(request, response){
    var oldPath = publicPath+'/'+'Main Screen.html';
    var normalisedPath = path.normalize(oldPath);
    response.sendFile(normalisedPath);
})

app.post('/login', function(request, response){
    var user = request.body.username; // making use of body-parser
    var pass = request.body.password;

    var message = "";
    userModel.findOne({"username":user}, function(err, userData){
        if(err){
            console.log(err);
            message = "Error: login failed - please refer to server console"
        } else {
            if(userData === null){
                message = "Login failed: Username does not exist";
                console.log(message);
            } else if(userData.password != pass){
                message = "Login failed: Incorrect password";
                console.log(message);
            } else {
                console.log("found user: " + user + " logging in...");
            }

        }
        response.send(message);
    })

})

app.get('/editorchoices', function(request, response){
    editorChoiceModel.find({}, function(err, editorChoices){
          getRecipesFromIds(editorChoices, response);
    })
})

function getRecipesFromIds(recipeids, response){
  var recipeNum = 0;
  var currentCount = 0;
  var recipeArray = [];
  recipeNum = recipeids.length;
  recipeids.forEach(function(idobject){
      id = idobject.recipe_id;

      if(id === undefined){
        id = idobject;
      }

      recipeModel.findById(id, function(err, recipe){
          if(err){
              console.log(err.message);
              return null;
          } else {
              miniRecipe = {"id":recipe._id,
                                "name":recipe.name,
                                "description": recipe.description,
                                "prep_time": recipe.prep_time,
                                "diet_info": recipe.diet_info,
                                "image_source": recipe.image_source};
              recipeArray.push(miniRecipe);
              currentCount = currentCount + 1;
              if(currentCount === recipeNum){
                  response.send(recipeArray);
              }
          }
      });
  })
}

app.get('/recommended', function(request, response){
  var username = request.query.username;
  userModel.findOne({"username":username}, function(err, user){
        if (err) {
            console.log("error retrieving recommended recipes: " + error);
            response.send("");
            return;
        }

        if(user === null){
            response.send("");
            return;
        }

        userRecommendationModel.findOne({"username":username}, function(err, userRecommendation){
            if(userRecommendation === undefined || userRecommendation === null){
                response.send("");
                return;
            }
            var recs = userRecommendation.recommendations;
            if(recs === null || recs === undefined || recs.length == 0 ){
                response.send("");
                return;
            }
            getRecipesFromIds(recs, response);
        });
  });
})

app.get('/followRecommended', function(request, response){
    response.send("");
    return;
})

app.get('/favourites', function(request, response){
    var user = request.query.username;
    userModel.findOne({"username":user}, function(err, user){
          if (err) {
              console.log("error retrieving favourited recipes: " + error);
              response.send("");
              return;
          }

          if(user === null){
              response.send("");
              return;
          }

          var favourites = user.favourites;

          if(favourites === null || favourites === undefined || favourites.length == 0 ){
              response.send("");
              return;
          }

          getRecipesFromIds(favourites, response);
    });
})

app.get('/saves', function(request, response){
    var user = request.query.username;

    userModel.findOne({"username":user}, function(err, user){
          if (err) {
              console.log("error retrieving saved recipes: " + error);
              response.send("");
              return;
          }

          if(user === null){
              response.send("");
              return;
          }


          var saved = user.saved;
          if(saved === null || saved === undefined || saved.length == 0){
              response.send("");
              return;
          }

          getRecipesFromIds(saved, response);
    });
})

app.get('/viewRecipe', function(request, response){
    var recipe_id =  request.query.recipe_id;
    recipeModel.findById(recipe_id, function(err, recipe){
        if(err){
            console.log(err.message);
        } else {
            var path = publicPath+'/'+'Recipe View Screen';
            response.render(path, {recipe : recipe}, function(err, html) {
                response.send(html);
            });
        }
    })
})

app.post('/saveRecipe', function(request, response){
    var username =  request.body.username;
    var recipe_id =  request.body.recipe_id;

    userModel.findOneAndUpdate(
     { username: username },
     { $push: { saved: recipe_id  } },
    function (error, success) {
        var message = "";
        if (error) {
            message = "error saving recipe: " + error;
            console.log(message);
        } else {
          message = "saved recipe for user:" + username;
          console.log(message);
          response.send(message);
        }
    });
})

app.post('/unsaveRecipe', function(request, response){
    var username =  request.body.username;
    var recipe_id =  request.body.recipe_id;

    userModel.findOneAndUpdate(
     { username: username },
     { $pull: { saved: recipe_id  } },
    function (error, success) {
      var message = "";
      if (error) {
          message = "error unsaved recipe: " + error;
          console.log(message);
      } else {
        message = "unsaved recipe for user:" + username;
        console.log(message);

        response.send(message);
      }
    });
})

app.post('/favouriteRecipe', function(request, response){
    var username =  request.body.username;
    var recipe_id =  request.body.recipe_id;

    userModel.findOneAndUpdate(
     { username: username },
     { $push: { favourites: recipe_id  } },
    function (error, success) {
      var message = "";
      if (error) {
          message = "error favouriting recipe: " + error;
          console.log(message);
      } else {
        message = "favourited recipe for user:" + username;
          response.send(message);
      }
    });
})

app.post('/unfavouriteRecipe', function(request, response){
    var username =  request.body.username;
    var recipe_id =  request.body.recipe_id;

    userModel.findOneAndUpdate(
     { username: username },
     { $pull: { favourites: recipe_id  } },
    function (error, success) {
      var message = "";
      if (error) {
          message = "error unfavouriting recipe: " + error;
          console.log(message);
      } else {
        message = "unfavourited recipe for user:" + username;
          console.log(message);
          response.send(message);
      }
    });
})

app.post('/recommendRecipe', function(request, response){
    var username =  request.body.username;
    var recipe_id =  request.body.recipe_id;

    userRecommendationModel.findOneAndUpdate(
     { username: username },
     { $push: { recommendations: recipe_id  } },
    function (error, success) {
        var message = "";
        if (error) {
            console.log("error saving recipe: " + error);
            return;
        } else {
            message = "successfully recommended recipe for user:" + username;
            if(success === null){
                var newRecommendation = new userRecommendationModel({username: username,
                recommendations: [recipe_id]});
                newRecommendation.save(function(error){
                    if(error){
                        console.log(error.message);
                        return;
                    } else {
                        message = "recommended recipe for user:" + username;
                        console.log(message);
                    }
                });
            }
            console.log(message);
            response.send(message);
            broadcastRecommendation(username, recipe_id);
        }
    });
})

function broadcastRecommendation(username, recipe_id){
    userFollowsModel.findOne(
    { username: username },
    function(err, userFollows){
          if (err) {
              console.log("error retrieving saved recipes: " + error);
              response.send("");
              return;
          }

          if(userFollows === null || userFollows === undefined){
              // no followers so do nothing
              return;
          }

          var currentUserFollows = userFollows.followData.followedBy;

          var i;
          var j;
          for(i = 0; i < currentUserFollows.length; i++){
              for(j = 0; j < connections.length; j++){
                  if(connections[j].username === currentUserFollows[i]){
                      msg = JSON.stringify({username: username, recipe_id: recipe_id});
                      connections[j].connection.sendUTF(msg);
                  }
              }
          }
    });
}

app.post('/unrecommendRecipe', function(request, response){
    var username =  request.body.username;
    var recipe_id =  request.body.recipe_id;

    userRecommendationModel.findOneAndUpdate(
     { username: username },
     { $pull: { recommendations: recipe_id  } },
    function (error, success) {
        var message = "";
        if (error) {
            message = "error saving recipe: " + error;
            console.log(message);
        } else {
            message = "unrecommended recipe for user:" + username;
            console.log(message);
            response.send(message);
        }
    });
})

app.post('/userRecipeCheck', function(request, response){
    var username =  request.body.username;
    var recipe_id =  request.body.recipe_id;

    result = {saved: 0, recommended: 0, favourited: 0};

    if(username == null){
        return response.send(result);
    }

    userModel.findOne(
     { username: username },
    function (error, user) {
        var message = "";
        if (error) {
            message = "error saving recipe: " + error;
            console.log(message);
        } else {
            var saved = user.saved.includes(recipe_id);
            var recd = false;
            var faved = user.favourites.includes(recipe_id);

            userRecommendationModel.findOne({"username":username}, function(err, userRecommendations){
                if(userRecommendations != undefined){
                    recd = userRecommendations.recommendations.includes(recipe_id);
                }

                result = {saved: saved,
                    recommended: recd,
                    favourited:  faved};
                response.send(result);
            });
        }
    });
})

app.get('/viewUser', function(request, response){
    var username =  request.query.username;

    userModel.findOne({username: username}, function(err, user){
        if(err){
            console.log(err.message);
        } else {
            var path = publicPath+'/'+'User View Screen';
            var safeUser = user;
            delete safeUser.password;
            response.render(path, {userDetails : safeUser}, function(err, html) {
                response.send(html);
            });
        }
    })
})

app.post('/followUser', function(request, response){
    var username =  request.body.username;
    var userToFollow =  request.body.userToFollow;

    // update/add new follow
    userFollowsModel.findOneAndUpdate(
     { username: username },
     { $push: { 'followData.follows': userToFollow  } },
    function (error, success) {
        var message = "";
        if (error) {
            message = "error following user: " + error;
            console.log(message);
        } else {
            message = "Current user: " + username + " now follows "+ userToFollow;
            if(success === null){
                var newUserFollow = new userFollowsModel({username: username,
                 followData: {follows : userToFollow}});
                newUserFollow.save(function(error){
                    if(error){
                        message = error.message;
                        response.send(message);
                        return;
                    }
                });
            }
        }
    });

    // update/add new follow
    userFollowsModel.findOneAndUpdate(
     { username: userToFollow },
     { $push: { 'followData.followedBy': username  } },
    function (error, success) {
        var message = "";
        if (error) {
            message = "error following user: " + error;
            console.log(message);
        } else {
            message = "User: " + userToFollow + " is now followed by "+ username;
            if(success === null){
                var newUserFollow = new userFollowsModel({username: userToFollow,
                followData: {followedBy : username}});
                newUserFollow.save(function(error){
                    if(error){
                        message = error.message;
                    }
                });
            }
            console.log(message);
            response.send(message);
        }
    });
})

app.post('/unfollowUser', function(request, response){
    var username =  request.body.username;
    var userToFollow =  request.body.userToFollow;

    // update/add new follow
    userFollowsModel.findOneAndUpdate(
     { username: username },
     { $pull: { 'followData.follows': userToFollow  } },
    function (error, success) {
        var message = "";
        if (error) {
            message = "error following user: " + error;
            console.log(message);
        } else {
            message = "Current user: " + username + " no longer follows "+ userToFollow;
        }
    });

    // update/add new follow
    userFollowsModel.findOneAndUpdate(
     { username: userToFollow },
     { $pull: { 'followData.followedBy': username  } },
    function (error, success) {
        var message = "";
        if (error) {
            message = "error following user: " + error;
            console.log(message);
        } else {
            message = "User: " + userToFollow + " is no longer followed by "+ username;
            console.log(message);
            response.send(message);
        }
    });
})

app.post('/userFollowCheck', function(request, response){
    var username =  request.body.username;
    var userToFollow =  request.body.userToFollow;

    userFollowsModel.findOne(
     { username: userToFollow },
    function (error, followInfo) {
        var message = "";
        if (error) {
            message = "error saving recipe: " + error;
            console.log(message);
        } else {
            var result = {following: false};
            if(followInfo != null && followInfo != undefined ){
                var currentUserFollows = followInfo.followData.followedBy.includes(username);
                result = {following: currentUserFollows, followInfo};
            }
            response.send(result);
        }
    });
})

app.get('/searchRecipes', function(request, response){
    var searchQuery = request.query.searchQuery;
    var path = publicPath+'/'+'Recipe Search Screen';
    response.render(path, {searchQuery : searchQuery}, function(err, html) {
        response.send(html);
    });
})

app.get('/getRecipes', function(request, response){
    var searchQuery = request.query.searchQuery;
    var recipeArray = [ ];
    recipeModel.find({ $or:[{ "name":{$regex : ".*"+searchQuery+".*", '$options' : 'i'}},
        {"description":{$regex : ".*"+searchQuery+".*", '$options' : 'i'} }] }, function(err, recipes){
        if(err){
            console.log(err.message);
        } else {
            var recipeNum = recipes.length;
            var currentCount = 0;
            recipes.forEach(function(recipe){
                miniRecipe = {"id":recipe._id,
                                  "name":recipe.name,
                                  "description": recipe.description,
                                  "prep_time": recipe.prep_time,
                                  "diet_info": recipe.diet_info,
                                  "image_source": recipe.image_source};
                recipeArray.push(miniRecipe);
                currentCount = currentCount + 1;
                if(currentCount === recipeNum){
                    response.send(recipeArray);
                }
            })

        }
    }).limit(10);
})

app.get('/searchUsers', function(request, response){
    var path = publicPath+'/'+'User Search Screen';
    response.render(path, function(err, html) {
        response.send(html);
    });
})

app.get('/getUsers', function(request, response){
    var searchQuery = request.query.searchQuery;
    var userArray = [ ];
    userModel.find({ $or:[{ "username":{$regex : ".*"+searchQuery+".*", '$options' : 'i'}}] }, function(err, users){
        if(err){
            console.log(err.message);
        } else {
            var userNum = users.length;
            var currentCount = 0;
            users.forEach(function(user){
                var safeUser = user;
                delete safeUser.password;
                userArray.push(safeUser);
                currentCount = currentCount + 1;
                if(currentCount === userNum){
                    response.send(userArray);
                }
            })
            if(userNum === 0){
                response.send("");
            }
        }
    }).limit(10);
})

app.get('/userRegistration', function(request, response){
    var path = publicPath+'/'+'User Registration Screen';
    response.render(path, function(err, html) {
        response.send(html);
    });
})

app.post('/registerUser', function(request, response){
    var username = request.body.username;
    var password = request.body.password;
    var description = request.body.description;

    userModel.findOne({"username":username}, function(err, userData){
        var message = "Registration Failed: Server error";
        if(err){
            console.log(err);
            message = "Error: Registration failed - please refer to server console"
        } else {
            if(userData === null){
                var newUser = new userModel({username: username, password: password, description:description});
                    newUser.save(function(error){
                        var message = "Registration failed: Server error";
                        if(error){
                            message = "Registration failed: Server error";
                            console.log(error.message);
                        } else {
                            message = "";
                            console.log("New user created: " + username);
                        }
                        response.send(message);
                        return;
                    });

            } else {
                message = "Registration failed: Username already exists";
                console.log(message);
                response.send(message);
            }
        }
    })
})

// Initialise editors' choices
editorChoiceModel.findOne({}, function(err, editorChoices){
    console.log("checking for existing editor's choices");
    if(editorChoices === null){
        console.log("editor choices uninitialised, initialising...")
        initialiseEditorChoices();
    }
})

function initialiseEditorChoices(){
    var r = recipeModel.find({}).limit(3);
    r.exec( function(err, recipes){
        recipes.forEach(function(recipe){
            var choice = new editorChoiceModel({recipe_id: recipe._id});
            choice.save(function(error){
                console.log("saved new editor's choice");
            });
        })
    });
}
