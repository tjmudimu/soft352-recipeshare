$( document ).ready(function() {
    $('#userSearchButton').toggleClass('disabled');

    $('.brandTextTop').click(function(){
        window.location.href = 'http://localhost:8000/';
    });

    var passwordBar = $('#loginPassword');
    passwordBar.keypress(function(e){
        var keyCode = e.which;
        // 13 === [enter] key
        if(keyCode === 13){
            attemptLogin();
        }
    });
    var usernameBar = $('#loginUsername');
    usernameBar.keypress(function(e){
        var keyCode = e.which;
        // 13 === [enter] key
        if(keyCode === 13){
            attemptLogin();
        }
    });

    /// search page search bar
    var newSearchBar = $('#newSearchBar');
    if(newSearchBar != null && newSearchBar != undefined ){
        var listHolder = $('#searchList');
        newSearchBar.keypress(function(e){
            var keyCode = e.which;
            // 13 === [enter] key
            if(keyCode === 13){
                listHolder.empty();
                retrieveUsers(newSearchBar.val());
            }
        });
        var newSearchButton = $("#newSearchButton");
        newSearchButton.click(function(){
            listHolder.empty();
            retrieveUsers(newSearchBar.val());
        });
    }
});

function retrieveUsers(searchQuery) {
    var currentUser = "";

    return $.get("http://localhost:8000/getUsers", { searchQuery : searchQuery }, function(data, status){
        if(data === ""){
            noUsersFound();
            return;
        }
        data.forEach(listUsers);
    });
}

function listUsers(user){
  var username = user["username"];
  var onclickCode = 'onclick="javascript:window.location.href = \'http://localhost:8000/viewUser?username='+username+'\';"';
  $('#searchList').append('<li class="searchLi" id="'+username+'" '+onclickCode+'>'+username+'</li>');
}
